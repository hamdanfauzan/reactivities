using System.ComponentModel.DataAnnotations;

namespace API.DTOs
{
    public class RegisterDto
    {
        [Required]
        public string DisplayName { get; set; }
        
        [Required]
        [EmailAddress] // berupa email
        public string Email { get; set; }
        
        [Required]
        [RegularExpression("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}", ErrorMessage = "Password must be complex")]
        // (?=.*\\d) -> any character and minimum has one number
        // (?=.*[a-z]) -> minimum has one character lowercase
        // (?=.*[A-Z]) -> minimum has one character uppercase
        // .{4,8} -> need to be in 4 to 8 characters long
        public string Password { get; set; }
        
        [Required]
        public string Username { get; set; }
    }
}