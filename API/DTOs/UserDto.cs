namespace API.DTOs
{
    //untuk informasi ke client ketika berhasil login
    public class UserDto
    {
        public string DisplayName { get; set; }
        public string Token { get; set; }
        public string Username { get; set; }
        public string Image { get; set; }
    }
}