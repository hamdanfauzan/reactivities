using System;
using System.Threading.Tasks;
using Application.Comments;
using MediatR;
using Microsoft.AspNetCore.SignalR;

namespace API.SignalR
{
    public class ChatHub : Hub
    {
        private readonly IMediator _mediator;

        public ChatHub(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task SendComment(Create.Command command)
        {
            //save comment to db and get CommentDto var
            var comment = await _mediator.Send(command);

            //kirim ke group, yaitu yg match dengan activityId
            await Clients.Group(command.ActivityId.ToString())
                .SendAsync("ReceiveComments", comment.Value);
        }

        //fungsi otomatis add ke group dan load comments ketika connect ke hub dengan parameter "activityId"
        public override async Task OnConnectedAsync()
        {
            var htppContext = Context.GetHttpContext();
            var activityId = htppContext.Request.Query["activityId"]; //hati-hati karena ini sensitive case
            await Groups.AddToGroupAsync(Context.ConnectionId, activityId);

            //get list of comments untuk client
            var result = await _mediator.Send(new List.Query { ActivityId = Guid.Parse(activityId) });

            //pilih clients mana yg akan dikirim comments. Yaitu ke caller
            await Clients.Caller.SendAsync("LoadComments", result.Value);
        }
    }
}