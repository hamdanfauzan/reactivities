import { format } from 'date-fns';
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import { Card, Image } from "semantic-ui-react";
import { useStore } from "../../app/stores/store";

export default observer(function ProfileEventPane() {
    const { profileStore } = useStore();
    const { events } = profileStore;

    return (
        <>
            {events.length > 0 && (
                <Card.Group itemsPerRow={4}>
                    {events.map(event => (
                        <Card as={Link} key={event.id} to={`/activities/${event.id}`}>
                            <Image src={`/assets/categoryImages/${event.category}.jpg`} fluid />
                            <Card.Content textAlign='center'>
                                <Card.Header>{event.title}</Card.Header>
                                <Card.Description>
                                    <p>{format(event.date!, 'do MMM')}</p>
                                    <p>{format(event.date!, 'H:mm aa')}</p>
                                </Card.Description>
                            </Card.Content>
                        </Card>
                    ))}
                </Card.Group>
            )}
        </>
    );
})