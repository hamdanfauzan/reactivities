import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Grid, Header, Tab } from "semantic-ui-react";
import { useStore } from "../../app/stores/store";
import ProfileEventPane from "./ProfileEventPane";

export default observer(function ProfileEvents() {
    const { profileStore } = useStore();
    const { setEventTab, loadingEvents } = profileStore;

    const panes = [
        { menuItem: 'Future Events', render: () => <ProfileEventPane /> },
        { menuItem: 'Past Events', render: () => <ProfileEventPane /> },
        { menuItem: 'Hosting', render: () => <ProfileEventPane /> }
    ]
    
    useEffect(() => {
        setEventTab(0);

        return () => {
            setEventTab(-1);
        }
    }, [setEventTab]);

    return (
        <Tab.Pane loading={loadingEvents}>
            <Grid>
                <Grid.Column width={16}>
                    <Header floated='left' icon='calendar' content='Activities' />
                </Grid.Column>
                <Grid.Column width={16}>
                    <Tab
                        menu={{ pointing: true, secondary: true }}
                        panes={panes}
                        onTabChange={(e, data) => profileStore.setEventTab(data.activeIndex)}
                    />
                </Grid.Column>
            </Grid>
        </Tab.Pane>
    )
})