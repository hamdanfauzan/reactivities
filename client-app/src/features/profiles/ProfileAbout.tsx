import { observer } from "mobx-react-lite";
import { useState } from "react";
import { Button, Grid, Header, Tab } from "semantic-ui-react";
import { Profile } from "../../app/models/profile";
import { useStore } from "../../app/stores/store";
import ProfileForm from "./ProfileForm";

interface Props {
    profile: Profile;
}

export default observer(function ProfileAbout({ profile }: Props) {
    const { profileStore: { isCurrentUser } } = useStore();

    const [editProfileMode, setEditProfileMode] = useState(false); //initial value editProfileMode = false

    return (
        <Tab.Pane>
            <Grid>
                <Grid.Column width={16}>
                    <Header floated='left' icon='user' content={`About ${profile.displayName}`} />
                    {isCurrentUser && (
                        <Button floated='right' basic
                            content={editProfileMode ? 'Cancel' : 'Edit Profile'}
                            onClick={() => setEditProfileMode(!editProfileMode)}
                        />
                    )}
                </Grid.Column>
                <Grid.Column width={16}>
                    {editProfileMode ? (
                        <ProfileForm profile={profile} setEditProfileMode={setEditProfileMode} />
                    ) : (
                        <p style={{whiteSpace: 'pre-wrap'}}>{profile.bio}</p> //menggunakan "{whiteSpace: 'pre-wrap'}" supaya bisa memunculkan new line pada paragraph
                        
                    )}
                </Grid.Column>
            </Grid>


        </Tab.Pane>
    )
})