import { Formik } from "formik";
import * as Yup from 'yup';
import { observer } from "mobx-react-lite";
import { Button, Form } from "semantic-ui-react";
import { Profile } from "../../app/models/profile";
import MyTextInput from "../../app/common/form/MyTextInput";
import MyTextArea from "../../app/common/form/MyTextArea";
import { useStore } from "../../app/stores/store";

interface Props {
    profile: Profile;
    setEditProfileMode: (editProfileMode: boolean) => void; //return void
}

export default observer(function ProfileForm({ profile, setEditProfileMode }: Props) {
    const { profileStore: { updateProfile } } = useStore();

    const validationSchema = Yup.object({
        displayName: Yup.string().required(),
    })
    
    function handleFormSubmit(profileForm: Partial<Profile>, setSubmitting: (isSubmitting: boolean) => void) {
        
        updateProfile(profileForm).then(() => {
            setSubmitting(false); //set value submitting in formik to false, sehingga loading berhenti
            setEditProfileMode(false);
        });
    }

    return (
        <Formik
            validationSchema={validationSchema}
            enableReinitialize
            initialValues={{
                displayName: profile?.displayName,
                bio: profile?.bio ?? ''
            }}
            onSubmit={(values, { setSubmitting }) => handleFormSubmit(values, setSubmitting)}>
            {({ handleSubmit, isValid, isSubmitting, dirty }) => (
                <Form className='ui form' onSubmit={handleSubmit} autoComplete='off'>
                    <MyTextInput name='displayName' placeholder='Display Name' />
                    <MyTextArea rows={3} name='bio' placeholder='Add your bio' />
                    <Button
                        disabled={isSubmitting || !dirty || !isValid}
                        loading={isSubmitting} floated='right' positive type='submit' content='Update Profile' />
                </Form>
            )}
        </Formik>
    )
})