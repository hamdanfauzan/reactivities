export interface ChatComment { //gunakan "ChatComment" karena semantic ui punya component "Comment"
    id: number;
    createdAt: Date;
    body: string;
    username: string;
    displayName: string;
    image: string;
}