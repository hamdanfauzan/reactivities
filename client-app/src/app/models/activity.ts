import { Profile } from "./profile";

export interface Activity {
    id: string;
    title: string;
    date: Date | null;
    description: string;
    category: string;
    city: string;
    venue: string;
    hostUsername: string;
    isCancelled: boolean;
    isGoing: boolean;
    isHost: boolean;
    host?: Profile;
    attendees: Profile[]
}

//untuk populate Activity dari ActivityFormValues
export class Activity implements ActivityFormValues {
    constructor(init?: ActivityFormValues) {
        Object.assign(this, init); //shortcut untuk get Activity dari ActivityFormValues dengan menggunakan .assign
    }
}

export class ActivityFormValues {
    id?: string = undefined;
    title: string = '';
    category: string = '';
    description: string = '';
    date: Date | null = null;
    city: string = '';
    venue: string = '';

    //membuat constructor ActivityFormValues dengan parameter activity karena ...
    //pada activity bisa terdapat host, attendees, dll. Hal ini akan membuat error ketika kita call API Activity/create atau Activity/edit 
    constructor(activity?: ActivityFormValues) {
        if (activity) {
            this.id = activity.id;
            this.title = activity.title;
            this.category = activity.category;
            this.description = activity.description;
            this.date = activity.date;
            this.venue = activity.venue;
            this.city = activity.city;
        }
    }
}