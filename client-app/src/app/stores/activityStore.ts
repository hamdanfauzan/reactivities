import { Pagination, PagingParams } from './../models/pagination';
import { Activity, ActivityFormValues } from './../models/activity';
import { makeAutoObservable, reaction, runInAction } from "mobx";
import agent from "../api/agent";
import { format } from 'date-fns';
import { store } from './store';
import { Profile } from '../models/profile';

export default class ActivityStore {
    activityRegistry = new Map<string, Activity>();
    selectedActivity: Activity | undefined = undefined;
    editMode = false;
    loading = false;
    loadingInitial = false; //diset false agar tidak ada loading terus-menerus ketika refresh page
    pagination: Pagination | null = null;
    pagingParams = new PagingParams();
    predicate = new Map().set('all', true); //menggunakan map supaya lebih mudah memanage filter 'isGoing', 'isHost', dan 'startDate'. Default predicate adalah 'all' atau show all Activities

    constructor() {
        makeAutoObservable(this);

        reaction(
            //cek jika ada perubahan pada setiap predicate.keys
            () => this.predicate.keys(),
            () => {
                this.pagingParams = new PagingParams();
                this.activityRegistry.clear();
                this.loadActivities(); //get data activities lagi sesuai axiosParams
            }
        )
    }

    setPagingParams = (pagingParams: PagingParams) => {
        this.pagingParams = pagingParams;
    }

    setPredicate = (predicate: string, value: string | Date) => {
        //fungsi ini untuk mengganti this.predicate.delete pada setiap case
        const resetPredicate = () => {
            this.predicate.forEach((value, key) => {
                if (key !== 'startDate') this.predicate.delete(key);
            })
        }
        switch (predicate) {
            case 'all':
                resetPredicate();
                this.predicate.set('all', true);
                break;
            case 'isGoing':
                resetPredicate();
                this.predicate.set('isGoing', true);
                break;
            case 'isHost':
                resetPredicate();
                this.predicate.set('isHost', true);
                break;
            case 'startDate':
                this.predicate.delete('startDate'); //delete dahulu lalu set supaya notice bahwa ada perubahan pada key tsb
                this.predicate.set('startDate', value);
                break;
        }
    }

    //axiosParams berupa computed property
    get axiosParams() {
        const params = new URLSearchParams();
        params.append('pageNumber', this.pagingParams.pageNumber.toString());
        params.append('pageSize', this.pagingParams.pageSize.toString());
        //loop pada predicate
        this.predicate.forEach((value, key) => {
            if (key === 'startDate') {
                //khusus date, diubah/diconvert ke iso string
                params.append(key, (value as Date).toISOString());
            } else {
                params.append(key, value);
            }
        })
        return params;
    }

    //activitiesByDate berupa computed property
    get activitiesByDate() {
        return Array.from(this.activityRegistry.values()).sort((a, b) => a.date!.getTime() - b.date!.getTime());
    }

    get groupedActivities() {
        return Object.entries(
            this.activitiesByDate.reduce((activities, activity) => {
                const date = format(activity.date!, 'dd MMM yyyy'); //represent key untuk grouping
                activities[date] = activities[date] ? [...activities[date], activity] : [activity]; //jika match date-nya, tambah ke list. Jika tidak match, buat ke list baru [activity]
                return activities;
            }, {} as { [key: string]: Activity[] }) //key berupa string dan value berupa array activity
        )
    }

    loadActivities = async () => {
        this.loadingInitial = true;
        try {
            const result = await agent.Activities.list(this.axiosParams);
            result.data.forEach(activity => {
                this.setActivity(activity);
            })
            this.setPagination(result.pagination);
            this.setLoadingInitial(false);
        } catch (error) {
            console.log(error);
            this.setLoadingInitial(false);
        }
    }

    setPagination = (pagination: Pagination) => {
        this.pagination = pagination;
    }

    loadActivity = async (id: string) => {
        let activity = this.getActivity(id);

        if (activity) {
            this.selectedActivity = activity;
            return activity;
        } else {
            this.loadingInitial = true;
            try {
                //memanggil ke agent
                activity = await agent.Activities.details(id);

                //tambahkan satu activity ke map 
                this.setActivity(activity);

                //gunakan runInAction karena kita mengubah observable values tanpa menggunakan 'action'
                runInAction(() => {
                    this.selectedActivity = activity;
                })
                this.setLoadingInitial(false);
                return activity;
            } catch (error) {
                console.log(error);
                this.setLoadingInitial(false);
            }
        }
    }

    private setActivity = (activity: Activity) => {
        const user = store.userStore.user;
        if (user) {
            //cek jika user berada dalam list attendees
            activity.isGoing = activity.attendees!.some(
                a => a.username === user.username
            )

            activity.isHost = activity.hostUsername === user.username;

            //get host dari list attendees yg sama dengan activity.hostUsername
            activity.host = activity.attendees?.find(x => x.username === activity.hostUsername);
        }
        activity.date = new Date(activity.date!);
        this.activityRegistry.set(activity.id, activity);
    }

    private getActivity = (id: string) => {
        return this.activityRegistry.get(id);
    }

    setLoadingInitial = (state: boolean) => {
        this.loadingInitial = state;
    }

    createActivity = async (activity: ActivityFormValues) => {

        //tidak menggunakan this.loading karena sudah bisa digantikan oleh isSubmitting dari Formik

        const user = store.userStore.user;
        const attendee = new Profile(user!);

        try {
            await agent.Activities.create(activity);
            const newActivity = new Activity(activity); //populate obj activity dari ActivityFormValues
            newActivity.hostUsername = user!.username;
            newActivity.attendees = [attendee];
            this.setActivity(newActivity);

            runInAction(() => {
                this.selectedActivity = newActivity;
            })
        } catch (error) {
            console.log(error);
        }
    }

    updateActivity = async (activity: ActivityFormValues) => {
        try {
            await agent.Activities.update(activity);
            runInAction(() => {
                if (activity.id) {
                    // updatedActivity terdiri dari properties this.getActivity(activity.id)
                    // ditambah dengan properties dari form yg telah diedit. Prop yg sama di activity akan override valuenya ke this.getActivity(activity.id)
                    let updatedActivity = { ...this.getActivity(activity.id), ...activity };

                    //karena belum terdeclare tipe dari updatedActivity dan kita tahu bahwa tipenya adalah Activity. Maka tambahkan "as Activity"
                    this.activityRegistry.set(activity.id, updatedActivity as Activity);
                    this.selectedActivity = activity as Activity;
                }
            })
        } catch (error) {
            console.log(error);
        }
    }

    deleteActivity = async (id: string) => {
        this.loading = true;
        try {
            await agent.Activities.delete(id);
            runInAction(() => {
                this.activityRegistry.delete(id);
                this.loading = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => {
                this.loading = false;
            })
        }
    }

    updateAttendance = async () => {
        const user = store.userStore.user;
        this.loading = true;
        try {
            // menggunakan "!" karena update attendance ini hanya dilakukan di halaman activity detail yg sudah pasti terset selectedActivity
            await agent.Activities.attend(this.selectedActivity!.id);
            runInAction(() => {
                if (this.selectedActivity?.isGoing) {
                    // remove user dari list attendees
                    this.selectedActivity.attendees = this.selectedActivity.attendees?.filter(a => a.username !== user?.username);

                    this.selectedActivity.isGoing = false;
                } else {
                    // add user to list attendees
                    const attendee = new Profile(user!);
                    this.selectedActivity?.attendees?.push(attendee);
                    this.selectedActivity!.isGoing = true;
                }

                // update activity pada registry
                this.activityRegistry.set(this.selectedActivity!.id, this.selectedActivity!);
            })
        } catch (error) {
            console.log(error);
        } finally {
            runInAction(() => {
                this.loading = false;
            })
        }
    }

    cancelActivityToggle = async () => {
        this.loading = true;
        try {
            await agent.Activities.attend(this.selectedActivity!.id);
            runInAction(() => {
                this.selectedActivity!.isCancelled = !this.selectedActivity?.isCancelled

                // update activity pada registry
                this.activityRegistry.set(this.selectedActivity!.id, this.selectedActivity!);
            })
        } catch (error) {
            console.log(error);
        } finally {
            runInAction(() => {
                this.loading = false;
            })
        }
    }

    updateAttendeeFollowing = (username: string) => {
        this.activityRegistry.forEach(activity => {
            activity.attendees.forEach(attendee => {
                if (attendee.username === username) {
                    attendee.following ? attendee.followersCount-- : attendee.followersCount++;
                    attendee.following = !attendee.following;
                }
            })
        })
    }

    clearSelectedActivity = () => {
        this.selectedActivity = undefined;
    }
}