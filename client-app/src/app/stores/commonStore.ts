import { ServerError } from './../models/serverError';
import { makeAutoObservable, reaction } from 'mobx';

export default class CommonStore {
    error: ServerError | null = null;
    token: string | null = window.localStorage.getItem('jwt');
    appLoaded = false;

    constructor() {
        makeAutoObservable(this);

        //gunakan reaction untuk token
        reaction(
            () => this.token, //define apa yg ingin direact
            token => {
                if (token) {
                    window.localStorage.setItem('jwt', token);
                } else {
                    window.localStorage.removeItem('jwt'); 
                }
            }
        )
    }

    setServerError = (error: ServerError) => {
        this.error = error
    }
    
    setToken = (token: string | null) => {
        this.token = token; //perubahan nilai pada this.token akan memanggil reaction 
    }

    setAppLoaded = () => {
        this.appLoaded = true;
    }
}