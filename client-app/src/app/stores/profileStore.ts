import { Activity } from './../models/activity';
import { makeAutoObservable, reaction, runInAction } from "mobx";
import agent from "../api/agent";
import { Photo, Profile } from "../models/profile";
import { store } from "./store";


export default class ProfileStore {
    profile: Profile | null = null;
    loadingProfile = false;
    uploading = false;
    loading = false;
    followings: Profile[] = [];
    loadingFollowings = false;
    activeTab = 0;

    events: Activity[] = [];
    loadingEvents = false;
    eventTab = -1; //tab pada halaman events

    constructor() {
        makeAutoObservable(this);

        reaction(
            () => this.activeTab,
            activeTab => {
                if (activeTab === 3 || activeTab === 4) {
                    const predicate = activeTab === 3 ? 'followers' : 'following';
                    this.loadFollowings(predicate);
                } else {
                    this.followings = [];
                }
            }
        )

        reaction(
            () => this.eventTab,
            eventTab => {
                const validTabs = [0, 1, 2];
                if (validTabs.includes(eventTab)) {
                    let predicate = '';
                    switch (eventTab) {
                        case 0:
                            predicate = 'future';
                            break;
                        case 1:
                            predicate = 'past';
                            break;
                        case 2:
                            predicate = 'hosting';
                            break;
                    }
                    this.loadEvents(predicate);
                } else {
                    this.events = [];
                }
            }
        )
    }

    setActiveTab = (activeTab: any) => {
        this.activeTab = activeTab;
    }

    setEventTab = (eventTab: any) => {
        this.eventTab = eventTab;
    }

    //new computed property
    get isCurrentUser() {
        if (store.userStore.user && this.profile) {
            return store.userStore.user.username === this.profile.username;
        }
        return false;
    }

    //new method
    loadProfile = async (username: string) => {
        this.loadingProfile = true;
        try {
            const profile = await agent.Profiles.get(username);
            runInAction(() => {
                this.profile = profile;
                this.loadingProfile = false;
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.loadingProfile = false)
        }
    }

    uploadPhoto = async (file: Blob) => {
        this.uploading = true;
        try {
            const response = await agent.Profiles.uploadPhoto(file);
            const photo = response.data
            runInAction(() => {
                if (this.profile) {
                    this.profile.photos?.push(photo);

                    //if main photo -> set main photo di store 
                    if (photo.isMain && store.userStore.user) {
                        store.userStore.setImage(photo.url);
                        this.profile.image = photo.url;
                    }
                }
                this.uploading = false;
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.uploading = false);
        }
    }

    setMainPhoto = async (photo: Photo) => {
        this.loading = true;
        try {
            await agent.Profiles.setMainPhoto(photo.id);

            //update prop di store lain
            store.userStore.setImage(photo.url);

            //runInAction untuk update prop di profileStore itself
            runInAction(() => {
                if (this.profile && this.profile.photos) {
                    this.profile.photos.find(p => p.isMain)!.isMain = false;
                    this.profile.photos.find(p => p.id === photo.id)!.isMain = true;
                    this.profile.image = photo.url;
                }
                this.loading = false;
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.loading = false);
        }
    }
    
    deletePhoto = async (photo: Photo) => {
        this.loading = true;
        try {
            await agent.Profiles.deletePhoto(photo.id);

            //runInAction untuk update prop di profileStore itself
            runInAction(() => {
                if (this.profile && this.profile.photos) {
                    //"filter" akan return array yg match condition, bukan mengubah array sebelumnya
                    this.profile.photos = this.profile.photos.filter(p => p.id !== photo.id);
                }
                this.loading = false;
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.loading = false);
        }
    }
    
    updateProfile = async (profile: Partial<Profile>) => {
        this.loading = true;
        try {
            await agent.Profiles.updateProfile(profile);

            //update prop di store lain
            if (profile.displayName && profile.displayName !== store.userStore.user?.displayName) {
                store.userStore.setDisplayName(profile.displayName); 
            }
            
            runInAction(() => {
                if (this.profile) {
                    // terdiri dari properties this.profile ditambah dengan properties dari form yg telah diedit
                    // Prop yg sama di profile akan override valuenya ke this.profile
                    this.profile = { ...this.profile, ...profile };
                }
                
                this.loading = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.loading = false);
        }
    }
    
    //parameter "following" = true jika ingin follow "username"
    updateFollowing = async (username: string, following: boolean) => {
        this.loading = true;
        try {
            await agent.Profiles.updateFollowing(username);
            store.activityStore.updateAttendeeFollowing(username);

            runInAction(() => {
                if (this.profile) {
                    //jika melihat profil orang lain
                    //dan jika orang yg difollow sama dengan orang pada profile page
                    if (this.profile.username !== store.userStore.user?.username && this.profile.username === username) {
                        following ? this.profile.followersCount++ : this.profile.followersCount--;
                        this.profile.following = !this.profile.following;
                    }

                    //jika melihat profile sendiri
                    if (this.profile.username === store.userStore.user?.username) {
                        following ? this.profile.followingCount++ : this.profile.followingCount--;
                    }

                    //cek setiap following
                    this.followings.forEach(profile => {
                        if (profile.username === username) {
                            //jika sebelumnya sudah follow user A (yg sesuai dg username) -> ketika kita update, akan menjadi tidak follow user A. Shg followers user A akan berkurang
                            profile.following ? profile.followersCount-- : profile.followersCount++;
                            profile.following = !profile.following;
                        }
                    })
                }
                
                this.loading = false;
            })
        } catch (error) {
            console.log(error);
            runInAction(() => this.loading = false);
        }
    }

    loadFollowings = async (predicate: string) => {
        this.loadingFollowings = true;
        try {
            const followings = await agent.Profiles.listFollowings(this.profile!.username, predicate);
            runInAction(() => {
                this.followings = followings;
                this.loadingFollowings = false;
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.loadingFollowings = false)
        }
    }

    loadEvents = async (predicate: string) => {
        this.loadingEvents = true;
        try {
            const events = await agent.Profiles.listActivities(this.profile!.username, predicate);
            events.forEach(event => {
                event.date = new Date(event.date!);
            })
            runInAction(() => {
                this.events = events;
                this.loadingEvents = false;
            });
        } catch (error) {
            console.log(error);
            runInAction(() => this.loadingEvents = false)
        }
    }
}