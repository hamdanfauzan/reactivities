import { Navigate, Outlet, useLocation } from "react-router-dom";
import { useStore } from "../stores/store";

export default function RequireAuth() {
    const {userStore: {isLoggedIn}} = useStore();
    const location = useLocation();

    if (!isLoggedIn) {
        //state "from" supaya tahu page sebelumnya user sebelum navigate ke home page ('/')
        return <Navigate to='/' state={{from: location}} />
    }

    return <Outlet />
}