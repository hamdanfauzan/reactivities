import { useEffect, useState } from "react";
import { Button, Grid, Header } from "semantic-ui-react";
import PhotoWidgetCropper from "./PhotoWidgetCropper";
import PhotoWidgetDropzone from "./PhotoWidgetDropzone";

interface Props {
    loading: boolean;
    uploadPhoto: (file: Blob) => void; //return void
}

export default function PhotoUploadWidget({loading, uploadPhoto}: Props) {
    //inisialisasi dengan empty array
    //"<any>" digunakan supaya bisa melakukan files[0].preview
    const [files, setFiles] = useState<any>([]);
    const [cropper, setCropper] = useState<Cropper>(); //menyimpan variable image image hasil crop

    //untuk ketika submit dan upload image hasil crop
    function onCrop() {
        if (cropper) {
            cropper.getCroppedCanvas().toBlob(blob => uploadPhoto(blob!));
        }
    }

    //untuk clean component dropzone setelah dispose
    useEffect(() => {
        return () => {
            files.forEach((file: any) => URL.revokeObjectURL(file.preview))
        }
    }, [files])

    return (
        <Grid>
            <Grid.Column width={4}>
                <Header sub color='teal' content='Step 1 - Add Photo' />
                <PhotoWidgetDropzone setFiles={setFiles} /> {/* fungsi setFiles digunakan di child component */}
            </Grid.Column>
            <Grid.Column width={1} />
            <Grid.Column width={4}>
                <Header sub color='teal' content='Step 2 - Resize image' />
                {files && files.length > 0 && (
                    <PhotoWidgetCropper setCropper={setCropper} imagePreview={files[0].preview} /> // pass parameter "setCropper" supaya PhotoWidgetCropper bisa update image hasil crop ke variable "cropper"
                )}
            </Grid.Column>
            <Grid.Column width={1} />
            <Grid.Column width={4}>
                <Header sub color='teal' content='Step 3 - Preview & Upload' />
                {files && files.length > 0 &&
                    <>
                        <div className='img-preview' style={{ minHeight: 200, overflow: 'hidden' }} /> {/* img-preview hasil preview dari PhotoWidgetCropper */}

                        <Button.Group widths={2}>
                            <Button loading={loading} onClick={onCrop} positive icon='check' />
                            <Button disabled={loading} onClick={() => setFiles([])} icon='close' /> {/* set files menjadi empty array */}
                            {/* disable button close ketika loading */}
                        </Button.Group>
                    </>}

            </Grid.Column>
        </Grid>
    )
}