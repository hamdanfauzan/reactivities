import { useLocation } from "react-router-dom";

//merupakan helper/custom hooks
export default function useQuery() {
    return new URLSearchParams(useLocation().search);
}