using System;

namespace Application.Comments
{
    public class CommentDto
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Body { get; set; }
        public string Username { get; set; } //username pengirim
        public string DisplayName { get; set; } //display name pengirim
        public string Image { get; set; } //user's profile image
    }
}