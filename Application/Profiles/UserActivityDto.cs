using System;
using System.Text.Json.Serialization;

namespace Application.Profiles
{
    public class UserActivityDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public DateTime Date { get; set; }

        //JsonIgnore digunakan untuk prop yg tidak akan ditampilkan ke client, tetapi masih dibutuhkan di API
        [JsonIgnore]
        public string HostUsername { get; set; }
    }
}