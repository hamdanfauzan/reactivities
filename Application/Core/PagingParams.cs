namespace Application.Core
{
    public class PagingParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        
        //menggunakan propfull backup
        private int _pageSize = 10; //jika tidak diset valuenya, pageSize akan bernilai 10
        public int PageSize
        {
            get => _pageSize; 
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
        
    }
}