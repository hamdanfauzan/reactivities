using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Followers
{
    public class List
    {
        public class Query : IRequest<Result<List<Profiles.Profile>>> //menggunakan "Profiles.Profile" karena AutoMapper juga memiliki class Profile. Supaya tidak ambigu
        {
            public string Predicate {get; set;} //untuk membedakan return list Following atau list Followers
            public string Username {get; set;}
        }

        public class Handler : IRequestHandler<Query, Result<List<Profiles.Profile>>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserAccessor _userAccessor;

            public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor)
            {
                _context = context;
                _mapper = mapper;
                _userAccessor = userAccessor;
            }

            public async Task<Result<List<Profiles.Profile>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var profiles = new List<Profiles.Profile>();
                
                switch (request.Predicate)
                {
                    case "followers":
                        profiles = await _context.UserFollowings
                            .Where(x => x.Target.UserName == request.Username)
                            .Select(u => u.Observer) //select followers dalam tipe AppUser nya saja
                            .ProjectTo<Profiles.Profile>(_mapper.ConfigurationProvider,
                                new {currentUsername = _userAccessor.GetUsername()}) //set property currentUsername di MappingProfile dengan menambah parameter baru
                            .ToListAsync(cancellationToken);
                        break;
                    case "following":
                        profiles = await _context.UserFollowings
                            .Where(x => x.Observer.UserName == request.Username)
                            .Select(u => u.Target) //select following dalam tipe AppUser nya saja
                            .ProjectTo<Profiles.Profile>(_mapper.ConfigurationProvider,
                                new {currentUsername = _userAccessor.GetUsername()}) //set property currentUsername di MappingProfile dengan menambah parameter baru)
                            .ToListAsync(cancellationToken);
                        break;
                }
                
                return Result<List<Profiles.Profile>>.Success(profiles);
            }
        }
    }
}