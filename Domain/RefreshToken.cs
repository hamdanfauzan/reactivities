namespace Domain
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public AppUser AppUser { get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; } = DateTime.UtcNow.AddDays(7); //7 hari cukup cocok untuk expire dari refresh token
        public bool IsExpired => DateTime.UtcNow >= Expires;
        public DateTime? Revoked { get; set; } //menyimpan info pernah direvoke di tgl kapan
        public bool IsActive => Revoked == null && !IsExpired; //jika IsActive -> boleh refresh JWT
    }
}